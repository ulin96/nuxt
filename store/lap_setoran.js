
export const state = () => ({
    lap_setorans: [], //DATA USER AKAN DISIMPAN KE DALAM STATE INI
    errors: [],
    page: 1,
    data:[] //DEFAULT PAGE YANG AKTIF ADALAH 1
})

export const mutations = {
    //MUTATION INI DIGUNAKAN UNTUK MENGUBAH VALUE DARI STATE USERS
    SET_USER_DATA(state, payload) {
        state.lap_setorans = payload
    },
    SET_ERRORS(state, payload) {
        state.errors = payload
    },
     //MUTATION UNTUK MENGUBAH VALUE DARI STATE PAGE
     SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_DATA(state, payload) {
        state.data = payload
    },
}

export const actions = {
    getLapSetData({ commit }, payload,kk) {
        let id = localStorage.getItem('id') ? localStorage.getItem('id'):''
         return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE SERVER UNTUK MENGAMBIL DATA USER
            this.$axios.get(`/laporan_set/${id}?jns=${kk}`).then((response) => {
                commit('SET_DATA', response.data.data) //SET DATA YANG DITERIMA KE DALAM STATE
                resolve()
            })
        })
    },
    //FUNGSI UNTUK MENGAMBIL DATA USER
    getlap_setoranData({ commit, state }, payload ) {
        let cek = payload?payload:''
        let search = cek.cari ? cek.cari:''
        let filter = cek.id_jenis_sampah ? cek.id_jenis_sampah:''
        
        let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'):''
        return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE API, DENGAN MENGIRMKAN PARAMETER Q DAN PAGE
            //DIMANA Q ADALAH PENCARIAN DAN PAGE ADALAH AKTIF PAGE YANG SEDANG DIAKSES
            this.$axios.get(`/setoran_siswa/${id}?q=${search}&jns=${filter}`).then((response) => {
                // if($auth.user.id_sekolah==response.data.data.id_sekolah){
                commit('SET_USER_DATA', response.data) //JIKA BERHASIL, SET DATA BARU 
                resolve()
                // }
            })
        })
    },
}