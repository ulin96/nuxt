export const state = () => ({
    datas: [], //DATA USER AKAN DISIMPAN KE DALAM STATE INI
    errors: [],
    cek:0,
    page: 1,
    data:[] //DEFAULT PAGE YANG AKTIF ADALAH 1
})

export const mutations = {
    //MUTATION INI DIGUNAKAN UNTUK MENGUBAH VALUE DARI STATE USERS
    SET_USER_DATA(state, payload) {
        // state.jeniss = payload
        state.datas = payload
    },
    SET_ERRORS(state, payload) {
        state.errors = payload
    },
     //MUTATION UNTUK MENGUBAH VALUE DARI STATE PAGE
     SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_DATA(state, payload) {
        state.data = payload
    },
}

export const actions = {
    getData({ commit }, payload) {
        return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE SERVER UNTUK MENGAMBIL DATA USER
            this.$axios.get(`/jenis/${payload}`).then((response) => {
                commit('SET_DATA', response.data.data) //SET DATA YANG DITERIMA KE DALAM STATE
                resolve()
            })
        })
    },
    //FUNGSI UNTUK MENGAMBIL DATA USER
    getTotalData({ commit, state }, payload) {
        let search = payload ? payload:''
        return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE API, DENGAN MENGIRMKAN PARAMETER Q DAN PAGE
            let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'):''
            this.$axios.get(`/dashboard/get_all/${id}?q=${search}`).then((response) => {
            // this.$axios.get(`/jeniss?q=${search}`).then((response) => {
                // if($auth.user.id_sekolah==response.data.data.id_sekolah){
                // commit('SET_USER_DATA', response.data.data) //JIKA BERHASIL, SET DATA BARU 
                commit('SET_USER_DATA', response.data) //JIKA BERHASIL, SET DATA BARU 
                console.log(response.data)
                resolve()
                // }
            })
        })
    },
}