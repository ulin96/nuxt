export const state = () => ({
    s_tarikSaldo: [],
    s_jenisTarik: [],
    response: []
})

export const mutations = {
    SET_TARIKSALDODATA(state, payload){
        state.s_tarikSaldo = payload
    },
    SET_JENISTARIK(state, payload){
        state.s_jenisTarik = payload
    },
    SET_RENPONSE(state, payload){
        state.response = payload
    }
}

export const actions = {
    getTarikSaldoData({commit}, payload) {
        return new Promise((resolve, reject) => {
            let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'): ''
            this.$axios.get(`/tarik_saldo_siswa/${id}`)
            .then((response) => {
                commit('SET_TARIKSALDODATA', response.data)
                resolve()
            })
        })
    },
    getJenisTarikSaldo({commit}) {
        return new Promise((resolve) => {
            this.$axios.get(`/tarik_saldo_siswa/jenis_tarik`)
            .then((response) => {
                commit('SET_JENISTARIK', response.data)
                resolve()
            })
            .catch((e) => {
                console.log(e)
            })
        })
    },
    storeTarikSaldo({dispatch, commit}, payload) {
        return new Promise((resolve) => {
            let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'): ''
            this.$axios.post(`/tarik_saldo_siswa/tarik/${id}`, payload)
            .then((response) => {
                commit('SET_RENPONSE', response.data)
                dispatch('getTarikSaldoData')
                resolve()
            })
            .catch((e) => {
                console.log(e)
            })
        })
    }
}