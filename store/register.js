export const state = () => ({
    s_lembaga: [],
    s_jenjang: [],
    s_provinsi: [],
    s_kabupaten: [],
    s_kecamatan: [],
    s_errors: []
})

export const mutations = {
    SET_JENJANG(state, payload){
        state.s_jenjang = payload
    },
    SET_LEMBAGA(state, payload){
        state.s_lembaga = payload
    },
    SET_PROVINSI(state, payload){
        state.s_provinsi = payload
    },
    SET_KABUPATEN(state, payload){
        state.s_kabupaten = payload
    },
    SET_KECAMATAN(state, payload){
        state.s_kecamatan = payload
    },
    SET_ERRORS(state, payload){
        state.s_errors = payload
    }
}

export const actions = {
    getProvinsiData({ commit }, payload) {
        return new Promise((resolve, reject) => {
           this.$axios.get(`/auth/provinsi`).then((response) => {
               commit('SET_PROVINSI', response.data)
               resolve()
           })
       })
   },
   getKabupatenData({ commit }, payload) {
        return new Promise((resolve, reject) => {
           this.$axios.get(`/auth/kabupaten/${payload}`).then((response) => {
               commit('SET_KABUPATEN', response.data)
               resolve()
           })
       })
   },
   getKecamatanData({ commit }, payload) {
        return new Promise((resolve, reject) => {
           this.$axios.get(`/auth/kecamatan/${payload}`).then((response) => {
               commit('SET_KECAMATAN', response.data)
               resolve()
           })
       })
   },
   getJenjangData({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.get(`/auth/jenjang`).then((response) => {
                commit('SET_JENJANG', response.data)
                resolve()
            }) 
        })
    },
   getLembagaData({commit}, payload) {
       return new Promise((resolve, reject) => {
           this.$axios.get(`/auth/lembaga/${payload}`).then((response) => {
               commit('SET_LEMBAGA', response.data)
               resolve()
           })
       })
   },
   storeRegisterData({dispatch, commit}, payload) {
       return new Promise((resolve, reject) => {
           this.$axios.post(`/auth/regis`, payload)
           .then(() => {
            //    dispatch
                resolve()
           })
           .catch((error) => {
               commit('SET_ERRORS', error.response.data)
           })
       })
   }
}
