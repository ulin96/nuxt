
export const state = () => ({
    siswas: [], //DATA USER AKAN DISIMPAN KE DALAM STATE INI
    errors: [],
    page: 1,
    data:[],
    tabungan:[],
    provinsis:[], //DEFAULT PAGE YANG AKTIF ADALAH 1
    kabupatens:[], //DEFAULT PAGE YANG AKTIF ADALAH 1
    kecamatans:[], //DEFAULT PAGE YANG AKTIF ADALAH 1
    response: []
})

export const mutations = {
    //MUTATION INI DIGUNAKAN UNTUK MENGUBAH VALUE DARI STATE USERS
    SET_PROVIN_DATA(state, payload) {
        state.provinsis = payload
    },
    SET_KAB_DATA(state, payload) {
        state.kabupatens = payload
    },
    SET_KEC_DATA(state, payload) {
        state.kecamatans = payload
    },
    SET_USER_DATA(state, payload) {
        state.siswas = payload
    },
    SET_ERRORS(state, payload) {
        state.errors = payload
    },
     //MUTATION UNTUK MENGUBAH VALUE DARI STATE PAGE
     SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_DATA(state, payload) {
        state.data = payload
    },
    SET_TABUNGAN(state, payload) {
        state.tabungan = payload
    },
    SET_RESPONSE(state, payload){
        state.response = payload
    }
}

export const actions = {
    getProvinsiData({ commit }, payload) {
         return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE SERVER UNTUK MENGAMBIL DATA USER
            this.$axios.get(`/daftar_siswa/provinsi`).then((response) => {
                commit('SET_PROVIN_DATA', response.data) //SET DATA YANG DITERIMA KE DALAM STATE
                resolve()
            })
        })
    },
    getKabupatenData({ commit }, payload) {
         return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE SERVER UNTUK MENGAMBIL DATA USER
            this.$axios.get(`/daftar_siswa/kabupaten/${payload}`).then((response) => {
                commit('SET_KAB_DATA', response.data) //SET DATA YANG DITERIMA KE DALAM STATE
                resolve()
            })
        })
    },
    getKecamatanData({ commit }, payload) {
         return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE SERVER UNTUK MENGAMBIL DATA USER
            this.$axios.get(`/daftar_siswa/kecamatan/${payload}`).then((response) => {
                commit('SET_KEC_DATA', response.data) //SET DATA YANG DITERIMA KE DALAM STATE
                resolve()
            })
        })
    },
    getSiswaData({ commit, state,rootState }, payload,context ) {
        let search = payload ? payload:''
        let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'):''
        return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE API, DENGAN MENGIRMKAN PARAMETER Q DAN PAGE
            //DIMANA Q ADALAH PENCARIAN DAN PAGE ADALAH AKTIF PAGE YANG SEDANG DIAKSES
            this.$axios.get(`/daftar_siswa/${id}?q=${search}`)
            .then((response) => {
                commit('SET_USER_DATA', response.data)
                resolve()
            })
        })
    },
    getData({ commit }, payload) {
         return new Promise((resolve, reject) => {
            this.$axios.get(`/daftar_siswa/detail/${payload}`).then((response) => {
                commit('SET_DATA', response.data)
                resolve()
            })
        })
    },
    getTabunganSiswa({commit}, payload) {
        return new Promise((resolve) => {
            this.$axios.get(`/daftar_siswa/tabungan/${payload}`)
            .then((response) => {
                commit('SET_TABUNGAN', response.data)
                resolve()
            })
        })
    },
    storeSiswaData({dispatch, commit}, payload) {
        return new Promise((resolve) => {
            let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'):''
            this.$axios.post(`/daftar_siswa/create/${id}`, payload,
            {
              headers: {
                  'Content-Type': 'multipart/form-data'
              }
            }
            )
            .then((response) => {
                dispatch('getSiswaData')
                commit('SET_RESPONSE', response.data)
                resolve()
            })
            .catch((error) => {
                console.log(error)
            })
        })
    },
    editDataSiswa({dispatch, commit}, payload){
        return new Promise((resolve) => {
            this.$axios.post(`/daftar_siswa/update/${payload.id_siswa}`, payload.formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            )
            .then((response) => {
                dispatch('getSiswaData')
                commit('SET_RESPONSE',response.data)
                resolve()
            })
            .catch((error) => {
                console.log(error);
            })
        })
    },
    deleteData({dispatch, commit }, payload) {
        return new Promise((resolve) => {
           this.$axios.put(`/daftar_siswa/delete/${payload}`)
            .then((response) => {
                dispatch('getSiswaData')
                commit('SET_RESPONSE', response.data)
                resolve()
            })
            .catch((error) => {
                console.log(error)
                commit('SET_ERRORS', error)
            })
       })
    },


}