export const state = () => ({
    s_lapPenjualan: []
})

export const mutations = {
    SET_PENJUALANDATA(state, payload){
        state.s_lapPenjualan = payload
    }
}

export const actions = {
    getLapPenjualanData({commit}, payload) {
        return new Promise((resolve, reject) => {
            let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'): ''
            this.$axios.get(`/penjualan_sampah/${id}`)
            .then((response) => {
                commit('SET_PENJUALANDATA', response.data)
                console.log(response.data);
                resolve()
            })
        })
    }
}