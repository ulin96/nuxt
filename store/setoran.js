
export const state = () => ({
    setorans: [], //DATA USER AKAN DISIMPAN KE DALAM STATE INI
    errors: [],
    page: 1,
    data:[], //DEFAULT PAGE YANG AKTIF ADALAH 1
    respon: []
})

export const mutations = {
    //MUTATION INI DIGUNAKAN UNTUK MENGUBAH VALUE DARI STATE USERS
    SET_USER_DATA(state, payload) {
        state.setorans = payload
    },
    SET_ERRORS(state, payload) {
        state.errors = payload
    },
     //MUTATION UNTUK MENGUBAH VALUE DARI STATE PAGE
     SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_DATA(state, payload) {
        state.data = payload
    },
    SET_RESPONSE(state, payload) {
        state.respon = payload
    },
}

export const actions = {
    getSetoranData({ commit }, payload) {
        let search = payload ? payload:''
        let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'):''
        return new Promise((resolve, reject) => {
            this.$axios.get(`/setoran_siswa/${id}?q=${search}`)
            .then((response) => {
                commit('SET_USER_DATA', response.data) 
                resolve()
            })
        })
    },
    getSetoranDetail( payload){
        return new Promise((resolve) => {
            this.$axios.get(`/setoran_siswa/detail/${payload}`)
            .then((response) => {
                commit('SET_DATA', response.data)
                // console.log(response.data);
                resolve()
            })
            .catch((error) => {
                console.log(error);
            })
        })
    },
    createSetoranSiswa({dispatch, commit}, payload) {
        let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'):''
        console.log(payload.siswa)
        return new Promise((resolve) => {
            this.$axios.post(`/setoran_siswa/${id}`, payload)
            .then((response) => {
                dispatch('getSetoranData')
                commit('SET_RESPONSE', response.data)
                resolve()
            })
            .catch((e) => {
                console.log(e)
            })
        })
    },
    createDetailSetoranSiswa({dispatch},payload) {
        // console.log(payload)
        return new Promise((resolve) => {
            this.$axios.post(`/setoran_siswa/detail/${payload}`)
            .then((response) => {
                dispatch('getSetoranData')
                resolve()
            })
            .catch((e) => {
                console.log(e)
            })
        })
    }
}