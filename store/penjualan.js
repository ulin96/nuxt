
export const state = () => ({
    penjualans: [], //DATA USER AKAN DISIMPAN KE DALAM STATE INI
    errors: [],
    page: 1,
    data:[] //DEFAULT PAGE YANG AKTIF ADALAH 1
})

export const mutations = {
    //MUTATION INI DIGUNAKAN UNTUK MENGUBAH VALUE DARI STATE USERS
    SET_USER_DATA(state, payload) {
        state.penjualans = payload
    },
    SET_ERRORS(state, payload) {
        state.errors = payload
    },
     //MUTATION UNTUK MENGUBAH VALUE DARI STATE PAGE
     SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_DATA(state, payload) {
        state.data = payload
    },
}

export const actions = {
    getData({ commit }, payload) {
         return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE SERVER UNTUK MENGAMBIL DATA USER
            this.$axios.get(`/lap_setoran/${payload}`).then((response) => {
                commit('SET_DATA', response.data.data) //SET DATA YANG DITERIMA KE DALAM STATE
                resolve()
            })
        })
    },
    //FUNGSI UNTUK MENGAMBIL DATA USER
    getPenjualanData({ commit, state,rootState }, payload,context ) {
        let search = payload ? payload:''
        let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'):''
        return new Promise((resolve, reject) => {
            //KIRIM REQUEST KE API, DENGAN MENGIRMKAN PARAMETER Q DAN PAGE
            //DIMANA Q ADALAH PENCARIAN DAN PAGE ADALAH AKTIF PAGE YANG SEDANG DIAKSES
            this.$axios.get(`/penjualan_sampah/${id}?q=${search}`).then((response) => {
                // if($auth.user.id_sekolah==response.data.data.id_sekolah){
                commit('SET_USER_DATA', response.data) //JIKA BERHASIL, SET DATA BARU 
                resolve()
                // }
            })
        })
    },
    storePenjualanData({ dispatch, commit }, payload) {
        return new Promise((resolve, reject) => {
          
            let id = this.$auth.$storage.getUniversal('key') ? this.$auth.$storage.getUniversal('key'):''
        //MENGIRIM REQUEST KE SERVER DENGAN METHOD POST DAN DATA DARI PAYLOAD
            this.$axios.post(`/penjualan_sampah/create/${id}`, payload)
            .then(() => {
                //JIKA BERHASIL, MAKA LOAD DATA TERBARU
                dispatch('getPenjualanData')
                resolve()
            })
            .catch((error) => {
              //JIKA TERJADI ERROR VALIDASI, SET STATE UNTUK MENAMPUNG DATA ERROR VALIDASINYA
                commit('SET_ERRORS', error.response.data)
            })
        })
    },
}