
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
      script:[
      { src : "/plugins/jquery/jquery.min.js" },
      { src : "/plugins/jquery-ui/jquery-ui.min.js" },
      { src : "/plugins/jquery.chained.js" },
      { src : "/plugins/bootstrap/js/bootstrap.bundle.min.js" },
      { src : "/plugins/chart.js/Chart.min.js" },
      { src : "/plugins/moment/moment.min.js" },
      { src : "/plugins/daterangepicker/daterangepicker.js" },
      { src : "/plugins/summernote/summernote-bs4.min.js" },
      { src : "/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js" },
      { src : "/dist/js/adminlte.js" },
      { src : "/dist/js/demo.js" },
      { src : "/plugins/datatables/jquery.dataTables.js" },
      { src : "/plugins/datatables-bs4/js/dataTables.bootstrap4.js" },
      { src : "/plugins/jquery-validation/jquery.validate.min.js", body: true },
      { src : "/plugins/sweetalert/sweetalert.min.js", body: true },
    ]
  },
  /*
  ** Customize the progress-bar color
  */

  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
 
  css: [
  '@/assets/plugins/fontawesome-free/css/all.min.css',
    '@/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
    '@/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
    '@/assets/plugins/jqvmap/jqvmap.min.css',
    '@/assets/dist/css/adminlte.min.css',
    '@/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
    '@/assets/plugins/daterangepicker/daterangepicker.css',
    '@/assets/plugins/summernote/summernote-bs4.css',
    '@/assets/plugins/ekko-lightbox/ekko-lightbox.css',
    '@/assets/plugins/sweetalert/sweetalert.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/axios.js',
    { src : '~/plugins/vue-apexcharts.js', ssr : false },
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
     '@nuxtjs/auth',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
 auth: {
  strategies: {
    //METHOD LOGIN YANG AKAN KITA GUNAKAN
    local: {
      //DIMANA ENDPOINTNYA ADALAH
      endpoints: {
        //UNTUK LOGIN PADA BAGIAN URL, KITA MASUKKAN URL LOGIN DARI API YANG SUDAH KITA BUAT
        //SEDANGKAN PROPERTYNAME ADALAH PROPERTY YANG INGIN KITA AMBIL VALUENYA
        //DALAM HAL INI, LOGIN MENGHARAPKAN TOKEN, SEDANGKAN PADA API KITA ME-RETURN TOKEN DI DALAM OBJECT DATA
        login: { url: '/auth/login', method: 'post', propertyName: 'token' },
        // login: { url: '/login', method: 'post', propertyName: 'token' },
        logout: { url: '/auth/logout', method: 'post' },
        user: false
      },
      tokenRequired: true,
      autoFetchUser:false,
      tokenType: 'Bearer'
    }
  }
},
//SET BASE URL PROJECT API KITA, SEHINGGA SEMUA REQUEST AKAN MENGARAH KESANA
axios: {
  //ci
  // baseURL: 'http://localhost/kompis/api/sekolah-api/index.php' 
  //lumen
  baseURL: 'http://kompis.online/apisek/index.php/' 

},
//MIDDLEWARE UNTUK MENGECEK SUDAH LOGIN ATAU BELUM, KITA SET GLOBAL
router: {
  middleware: ['auth']
},
  /*
  ** Build configuration
  */
  build: {
    // vendor : ['vue-apexcharts'],
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
